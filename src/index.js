import Vue from 'vue';

import './stylesheets/reset.css';
import './stylesheets/fonts.css';
import './stylesheets/main.css';
import './stylesheets/utilities.scss';
import './stylesheets/transitions.css';

import VueScrollTo from 'vue-scrollto';
import GreenTeamAtoms from './components/atoms';
import App from './app.vue';

import router from './routes';

Vue.use(GreenTeamAtoms);
Vue.use(VueScrollTo, {
  duration: 1500
})

new Vue({
	router,
	render(h) {
		return h(App)
	}
}).$mount('#js-app')