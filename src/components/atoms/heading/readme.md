Name of component: 'Heading'.
This component has 'mode' number prop, which will add html markup and BEM classes to created heading.
Also this component has 'uppercase' boolean prop, which adds BEM class, uppercasing inner text.