import button from './button';
import { container, row, col } from './grid';
import heading from './heading';
import input from './input';
import link from './link';
import paragraph from './paragraph';
import select from './select';

const GreenTeamAtoms = {
  install(Vue) {
    Vue.component('Button', button);
    Vue.component('Col', col);
    Vue.component('Container', container);
    Vue.component('Row', row);
    Vue.component('Heading', heading);
    Vue.component('Input', input);
    Vue.component('Link', link);
    Vue.component('Paragraph', paragraph);
    Vue.component('Select', select);
  }
}

export default GreenTeamAtoms;