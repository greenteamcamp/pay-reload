Name of component: 'Col'.
this component has 'col' boolean prop, which will add simple 'col' class to component.
There are also 'xs', 'sm', 'md', 'lg' and 'xl' string props, which will add classes according to modifications you provide 
Examples: (sm="auto", lg="9", "10", etc.)