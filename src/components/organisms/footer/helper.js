const contacts = [
  {
    heading: '+7 727 258-53-34',
    paragraph: 'Для жителей г. Алматы',
    src: '',
    alt: ''
    },
  {
    heading: '585-334',
    paragraph: 'В других городах',
    src: '',
    alt: ''
    },
  {
    heading: '7111',
    paragraph: 'С мобильного',
    src: '',
    alt: ''
    },
  {
    heading:'',
    paragraph: 'Звонок в Skype',
    src: '../../../../assets/images/skype-small.png',
    alt: 'Skype'
    },
  {
    heading: '',
    paragraph: 'Оставить отзыв',
    src: '../../../../assets/images/cloud-small.png',
    alt: 'Feedback'
  }
]

export default contacts;
