const placeholders = [
  {
    placeholder: 'Адрес', 
    autocomplete: 'on'
    },
  {
    placeholder: 'Торговое название',
    autocomplete: 'off'
    },
  {
    placeholder: 'Вид деятельности',
    autocomplete: 'off'
    },
  {
    placeholder: 'Режим работы',
    autocomplete: 'on'
    }
]

export default placeholders;