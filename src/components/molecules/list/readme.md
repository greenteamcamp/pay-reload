List component

it can be used as:
<List 
	title=""
	type=""
	:items="[]"
	ordered
	horizontal
	marginTop/>

It has props:
title: if your list has a title element
type: can be 'link','input','text'
items: string - if text, or an object with href and title, if link
ordered: use this boolean if you want ordered list
horizontal: use this boolean if you want list elements to display left-to-right
marginTop(-Right,-Bottom,-Left): string property that takes values [8,16,24,32,64]. Use it to space <li> elements