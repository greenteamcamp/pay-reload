import Vue from 'vue';
import VueRouter from 'vue-router';

import Pay from 'Pages/pay';
import Order from 'Pages/order';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    component: Pay
  },
  {
    path: '/order',
    component: Order
  }
];

export default new VueRouter({
  mode: 'history',
  routes,
  scrollBehavior: function(to, from, savedPosition) {
    if (to.hash) {
      return {selector: to.hash}
    } else {
      return {x: 0, y: 0}
    }
  }
});
